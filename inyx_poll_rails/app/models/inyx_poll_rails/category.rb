module InyxPollRails
	require 'elasticsearch/model'
  class Category < ActiveRecord::Base
  	has_many :polls, dependent: :destroy
	  include Elasticsearch::Model
	  include Elasticsearch::Model::Callbacks
	  validates_presence_of :name, :description
	  before_save :create_permalink

	  def create_permalink
	  	self.permalink=self.name.downcase.parameterize
	  end

  	def self.index(current_user)
  		Category.all
  	end

  	def self.query(query)
			{ query: { multi_match:  { query: query, fields: [:name, :description] , operator: :and }  }, sort: { id: "desc" }, size: Category.count }
		end

		def self.index_total(objects, current_user)
    	objects.count
  	end

  	def self.index_search(objects, current_user)
    	objects.all.order("created_at DESC")
  	end

  	def as_json(options = {})
      {
        id: self.id,
        name: self.name,
        description: self.description,
        permalink: self.permalink
      }
    end

    def self.route_index
    	"/admin/polls/categories"
  	end

  	def index_elasticsearch_document
	    self.__elasticsearch__.index_document
	  end
	  
	  def self.multiple_destroy(ids, current_user)
	    Category.destroy ids
	  end

  end
  Category.import
end

