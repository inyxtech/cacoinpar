class ApplicationMailer < ActionMailer::Base
  default from: "cacoinpar.org"
  layout 'mailer'
end
