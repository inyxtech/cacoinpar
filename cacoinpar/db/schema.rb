# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150525142241) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "affiliates", force: :cascade do |t|
    t.string   "status"
    t.boolean  "show_lading"
    t.string   "name"
    t.string   "date_construction"
    t.string   "acronym"
    t.string   "rif"
    t.string   "township_construction"
    t.string   "adress"
    t.string   "email_company"
    t.string   "phone"
    t.string   "mobile"
    t.string   "fax"
    t.string   "twitter"
    t.string   "facebook"
    t.string   "instagram"
    t.text     "organization"
    t.text     "services"
    t.integer  "user_id"
    t.string   "names"
    t.string   "charges"
    t.string   "email"
    t.string   "whit_representante"
    t.string   "phone_representante"
    t.string   "mobile_representante"
    t.string   "acta"
    t.string   "image_rif"
    t.string   "logo"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "branches", force: :cascade do |t|
    t.string   "address"
    t.string   "phone"
    t.integer  "affiliate_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "contact"
    t.string   "charge"
    t.string   "phone"
    t.integer  "affiliate_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inyx_blog_rails_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inyx_blog_rails_posts", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "subcategory_id"
    t.text     "content"
    t.string   "image"
    t.boolean  "public"
    t.boolean  "comments_open"
    t.boolean  "likes_enabled"
    t.boolean  "shared_enabled"
    t.string   "permalink"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inyx_blog_rails_subcategories", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inyx_catalog_rails_attachments", force: :cascade do |t|
    t.string   "name"
    t.string   "upload"
    t.text     "description"
    t.string   "image"
    t.text     "url"
    t.text     "target"
    t.integer  "catalog_id"
    t.boolean  "public"
    t.string   "permalink"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inyx_catalog_rails_catalogs", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "cover"
    t.string   "image_banner"
    t.boolean  "show_banner"
    t.string   "category"
    t.boolean  "public"
    t.string   "permalink"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inyx_poll_rails_answers", force: :cascade do |t|
    t.integer  "poll_id"
    t.integer  "option_id"
    t.integer  "user_id"
    t.string   "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inyx_poll_rails_categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "permalink"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "inyx_poll_rails_options", force: :cascade do |t|
    t.string   "answer"
    t.integer  "poll_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inyx_poll_rails_polls", force: :cascade do |t|
    t.string   "question"
    t.text     "description"
    t.date     "date_init"
    t.date     "date_end"
    t.integer  "category_id"
    t.boolean  "multiple_answer"
    t.boolean  "public"
    t.boolean  "restricted"
    t.string   "permalink"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "ip"
    t.string   "country_code"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "partners", force: :cascade do |t|
    t.string   "manes"
    t.string   "writ"
    t.string   "nationality"
    t.integer  "affiliate_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "representatives", force: :cascade do |t|
    t.string   "manes"
    t.string   "writ"
    t.string   "phone"
    t.string   "email"
    t.integer  "affiliate_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "permalink"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
