Rails.application.routes.draw do

  resources :affiliates, only: [:new, :create]



  root to: 'frontend#index'

  devise_for :users, skip: [:registration]

  mount InyxBlogRails::Engine, :at => '', as: 'post'
  mount InyxPollRails::Engine, :at => '', as: 'poll'
  mount InyxCatalogRails::Engine, :at => '', as: 'catalog'

  resources :admin, only: [:index]

  scope :admin do
    get 'news/:id', to: "admin#news_show", as: :news_show
    get 'polls/list/questions', to: "admin#polls", as: :polls_question

  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end

  scope :admin do
    resources :affiliates, except: [:new, :create] do
      collection do
        get '/download/:id/:type', to: 'affiliates#download_file', as: :download_attachment
        post '/delete', to: 'affiliates#delete'
      end
    end 
  end
  
end
