navbarbutton = ->
	buttonNavbar = 0
	$(".button-navbar").click ->
		if buttonNavbar is 0
			buttonNavbar = 1
			$(".sidebar-modal").addClass "show-modal"			
			$(".sidebar").addClass("show")
			$("body").css("overflow", "hidden");

			#script para cerrar con click al modal
			$(".show-modal").click ->
				$(".sidebar").removeClass "show"
				$(".sidebar-modal").removeClass "show-modal"
				$("body").css "overflow", "visible"
				buttonNavbar = 0
				return
		else
			buttonNavbar = 0
			$(".sidebar").removeClass "show"
			$(".sidebar-modal").removeClass "show-modal"
			$("body").css("overflow", "visible");
		return

	return


$(document).ready navbarbutton
$(document).on "page:load", navbarbutton