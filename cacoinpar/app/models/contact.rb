	class Contact < ActiveRecord::Base
	belongs_to :affiliate
	validates_presence_of :contact, :charge, :phone
	validates_numericality_of :phone, :allow_blank => true
	validates :phone, :length => { minimum: 11, maximum: 11 }
end
