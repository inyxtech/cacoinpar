InyxPollRails::Engine.routes.draw do
  scope :admin, :polls do
  	resources :categories do
  		collection do
	  		post '/delete', to: 'categories#delete'
  		end
  	end  	
  end

  scope "admin/polls/:permalink" do
    resource :answers, only: [:new, :create]
  end

  scope :admin do
  	resources :polls do
  		collection do
	  		post '/delete', to: 'polls#delete'
  		end
  	end
  end

  #get '/polls', to: 'polls#front_index', as: 'front_index'
  get '/admin/polls/:permalink/statistics', to: 'polls#statistics', as: 'statistics'

end
