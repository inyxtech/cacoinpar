## Encuesta 

Es un Engine que proporciona un módulo de encuesta que se integra a inyxmater admin

## Requerimientos

* Ruby >= 1.9.3
* Rails >= 3.0.0

## Instalación

Añadir la siguiente linea a su Gemfile

```ruby
gem 'inyx_poll_rails', path: '../inyx_poll_rails'
```

Ubicarse en la ruta del proyecto desde la terminal y ejecutar

```ruby
Bundle install
```

```ruby
rake inyx_poll_rails:install:migrations
rake db:migrate
```

añadir la siguiente ruta a su archivo routes.rb

```ruby
mount InyxPollRails::Engine, :at => '', as: 'poll'
```

## Configuración

Para agregar `config/initializers/inyx_poll_rails.rb` y asi estabelecer los datos de configuración, debe ejecutar

```ruby
rake poll:copy_initializer
```
## Vistas

Para copiar las vistas en `app/views/inyx_poll_rails` y asi personalizarlas para adaptarlas a sus necesidades, debe ejecutar

```ruby
rake poll:copy_views
```