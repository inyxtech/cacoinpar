module InyxPollRails
	require 'elasticsearch/model'
  class Poll < ActiveRecord::Base
    has_many :answers
    has_many :options
    accepts_nested_attributes_for :options
    belongs_to :category
    attr_accessor :options_attributes
  	include Elasticsearch::Model
	  include Elasticsearch::Model::Callbacks
	  validates_presence_of :question, :description, :date_init, :date_end, :category_id
    validates_uniqueness_of :question 
	  before_save :create_permalink
    before_validation :ignore_option_blank, :if => Proc.new {  |poll| poll.options_attributes? }
    after_save :crud_options, :if => Proc.new {  |poll| poll.options_attributes? }
    validate :validates_dates_poll, :validate_two_options, :validate_options_destroy

  	def create_permalink
	  	self.permalink=self.question.downcase.parameterize
	  end

	  def as_json(options = {})
      {
        id: self.id,
        question: self.question,
        description: self.description,
        permalink: self.permalink,
        public: self.public ? "Publicado": "No Publicado",
        restricted: self.restricted ? "Solo usuarios registrados" : "Todo público",
        multiple_answer: self.multiple_answer ? "Múltiples respuestas" : "Solo una respuesta",
        date_init: self.date_init.strftime("%d-%m-%Y"),
        date_end: self.date_end.strftime("%d-%m-%Y"),
        category: self.category.name
      }
    end
  	
  	def self.index(current_user)
  		Poll.all
  	end

  	def self.query(query)
			{ query: { multi_match:  { query: query, fields: [:question, :description, :date_init, :date_end] , operator: :and }  }, sort: { id: "desc" }, size: Poll.count }
		end

		def self.index_total(objects, current_user)
			objects.count
  	end

  	def self.index_search(objects, current_user)
    	objects.all.order("created_at DESC")
  	end

  	def self.route_index
    	"/admin/polls"
  	end

  	def index_elasticsearch_document
	    self.__elasticsearch__.index_document
	  end

	  def self.multiple_destroy(ids, current_user)
	    Poll.destroy ids
	  end

     def vote?(current_user)
      self.answers.find_by_user_id(current_user.id).nil?
    end

    def pie_chart_users
      hash = {"Usuarios registrados" => 0, "Usuarios no registrados" => 0}
      self.answers.each do |item|
        item.user_id.nil? ? hash["Usuarios no registrados"] += 1 : hash["Usuarios registrados"] += 1
      end
      hash
    end

    #validates custom
    def validates_dates_poll
      errors.add(:date_end, 'Fecha invalida' ) if (self.date_init > self.date_end) or (self.date_end < Date.today)
    end

    def validate_two_options 
      if !self.options_attributes? 
         errors.add(:base_option, 'Debe agregar opciones.')
      elsif self.options_attributes.count < 2
         errors.add(:base_option, 'Debe contener mínimo dos opciones.')
      end
    end

    def validate_options_destroy
      errors.add(:base_destroy_options, 'Debe dejar al menos dos opciones para la encuesta.') if self.created_at? and self.options_attributes and self.options.count >= 2 and self.options.count-self.options_destroy_count < 2
    end

    def options_destroy_count
      self.options_attributes.inject(0) do |count, (v, k)| 
        count += 1 if k["_destroy"] == "1"
        count
      end
    end

    #manage options
    def crud_options
      self.options_attributes.each do |key, value|
        if create_option?(key)
          self.options_attributes[key].delete "_destroy"         
          create_option self.options_attributes[key].merge(poll_id: self.id)
        elsif update_option?(key)
          self.options_attributes[key].delete "_destroy"
          update_option value["id"], self.options_attributes[key]
        elsif destroy_option?(key)
          delete_option value["id"]
        end
      end
    end

    def ignore_option_blank
      self.options_attributes.each do |key, value|
        self.options_attributes.delete key if value["answer"].blank?
      end
      self.options_attributes = nil if self.options_attributes.empty?
    end

    def options_attributes?
      !self.options_attributes.nil?
    end

    private

    def marked_for_destroy?(option)
      self.options_attributes[option]["_destroy"] == "1"
    end

    def create_option(params)
      Option.create params
    end

    def update_option(id, params)
      Option.find(id).update_attributes params
    end

    def delete_option(id)
      Option.delete id
    end

    def create_option?(option)
      self.options_attributes[option]["id"].nil? and !marked_for_destroy?(option)
    end

    def update_option?(option)
      self.options_attributes[option]["id"] and !marked_for_destroy?(option)
    end

    def destroy_option?(option)
      self.options_attributes[option]["id"] and marked_for_destroy?(option)
    end

  end
  Poll.import
end
