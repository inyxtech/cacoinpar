class CreateInyxPollRailsAnswers < ActiveRecord::Migration
  def change
    create_table :inyx_poll_rails_answers do |t|
      t.integer :poll_id
      t.integer :option_id
      t.integer :user_id
      t.string :ip

      t.timestamps null: false
    end
  end
end
