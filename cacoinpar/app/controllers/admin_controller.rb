class AdminController < ApplicationController
	before_filter :authenticate_user!
	layout 'admin/application'
  def index
  	unless params[:query].blank?
  		@posts = InyxBlogRails::Post.search(InyxBlogRails::Post.query params[:query]).records.where(likes_enabled: true).order('created_at DESC').page(params[:page]).per(10)
  	else
  		@posts = InyxBlogRails::Post.where(likes_enabled: true).order('created_at DESC').page(params[:page]).per(10)
  	end
  end

  def polls
    @polls = InyxPollRails::Poll.where("public = ? and date_init <= ? and date_end >= ?" , true, Date.today, Date.today).order('created_at DESC').page(params[:page]).per(10)
  end

  def news_show
  	@post = InyxBlogRails::Post.find(params[:id])
  end
end
