module InyxPollRails
  class Option < ActiveRecord::Base
  	has_many :answers
  	belongs_to :poll
  end
end
