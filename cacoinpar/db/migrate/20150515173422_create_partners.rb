class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :manes
      t.string :writ
      t.string :nationality
      t.belongs_to :affiliate

      t.timestamps null: false
    end
  end
end
