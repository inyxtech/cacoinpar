module InyxPollRails
  class Answer < ActiveRecord::Base
  	belongs_to :option
  	belongs_to :poll
  	validate :include_answer?, :restricted_current_user?
    validates_presence_of :poll_id, :option_id, :ip

    def include_answer?
      errors.add(:answer, 'Error: La respuesta no esta incluida en las opciones') unless self.poll.options.ids.include?(self.option_id)
    end

    def restricted_current_user?
    	errors.add(:restriced, 'Error: Debe estar Logeado para poder votar en esta encuesta') if self.poll.restricted and !self.user_id
    end

    def self.route_index
    	'/admin/polls'
    end

  end
end