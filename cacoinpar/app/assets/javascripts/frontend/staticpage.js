function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

$( document ).ready(function() {
	$('.top-nav').onePageNav({
	  currentClass: 'current',
	  changeHash: false,
	  scrollSpeed: 500,
	  scrollThreshold: 0.5,
	  filter: ':not(.external)',
	  easing: 'swing',
	  begin: function() {

	  },
	  end: function() {   

	  },
	  scrollChange: function($currentListItem) {      
	    		  
	  }
	});
});