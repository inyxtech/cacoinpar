module InyxPollRails
  class AnswersController < ApplicationController
  	layout 'admin/application'
  	before_action :get_poll
  	before_action :validates_all, :validates_multiple_answer, only: [:create]
    load_and_authorize_resource

  	def new
      @poll = Poll.find_by_permalink(params[:permalink])
        #(@poll.answers.find_by ip: request.remote_ip and !@poll.restricted) or
  		if (current_user and !@poll.answers.find_by(user_id: current_user.id).nil?)
  			redirect_to "/admin/polls/list/questions", alert: 'Ya has respondido en la encuesta seleccionada' 
      elsif @poll.restricted and !current_user
        redirect_to "/admin/polls/list/questions", alert: 'Debes estar registrado para responder esta encuesta.' 
  		else
  			@answer = Answer.new
  		end
  	end

  	def create
  		params[:answer][:answers].each do |opcion|
        @answer = Answer.new({poll_id: @poll.id, option_id: opcion, ip: request.remote_ip, user_id: (current_user.id if current_user)})
        @answer.save
      end
      return redirect_to "/admin/polls/list/questions", notice: 'Su respuesta ha sido guardado, gracias'
  	end

  	private
  		def get_poll
  			@poll = Poll.find_by_permalink(params[:permalink])
  		end

  	 # Only allow a trusted parameter "white list" through.
      def answer_params
        params.require(:answer).permit(answers: [])
      end

      #validations
      def validates_all
      	if params[:answer].nil?
      		return redirect_to new_answers_path(@poll.permalink), alert: 'Debe seleccionar una Respuesta.' 
      	else
	      	params[:answer][:answers].inject(false) do |validate, option|
		  			@answer = Answer.new(option_id: option, poll_id:  @poll.id, ip: request.remote_ip, user_id: (current_user.id if current_user))
						return redirect_to new_answers_path(@poll.permalink), alert: 'Los datos son invalidos.'  unless @answer.valid? 
						validate 
		  		end
		  	end
      end

      def validates_multiple_answer
      	if !@poll.multiple_answer and params[:answer][:answers].count > 1 
      		return redirect_to new_answers_path(@poll.permalink), alert: 'Debe guardar una sola Respuesta.' 
      	end
      end

  end
end