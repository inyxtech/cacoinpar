require "inyx_poll_rails/engine"

module InyxPollRails

	DROPDOWN = [{'poll' =>['<i class="fa fa-question"></i> Encuestas' , 'inyx_poll_rails/polls', 'inyx_poll_rails/categories']}, {'inyx_poll_rails/polls' => '/polls', 'inyx_poll_rails/categories' => '/polls/categories'}]

	def self.setup
		yield self
	end

end
