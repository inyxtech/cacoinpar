class Affiliate < ActiveRecord::Base
	include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks 
	
	has_many :partners, dependent: :destroy
	accepts_nested_attributes_for :partners, :allow_destroy => true
	has_many :contacts, dependent: :destroy
	accepts_nested_attributes_for :contacts, :allow_destroy => true
  belongs_to :user

  validates_presence_of :name, :date_construction, :rif, :adress, :email_company, :phone, :names, :charges, :email, :whit_representante, :acta, :image_rif, :logo
  validates :rif, :format => {:with => /^[a-zA-Z0-9]*$/, :multiline => true, :message => "No se aceptan caracteres especiales"}
  validates :email_company, :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :message => "Introduzca un email correcto" }
  validates_numericality_of :phone, :fax, :mobile, :mobile_representante, :phone_representante, :allow_blank => true
  validates :fax, :mobile, :mobile_representante, :phone_representante, :length => { minimum: 11, maximum: 11 }, :allow_blank => true
  validates :phone, :length => { minimum: 11, maximum: 11 }
  #validation custom
  validate :check_associations_number, on: :update
  validate :association_exist, on: :create

  mount_uploader :acta, DocumentUploader
	mount_uploader :image_rif, DocumentUploader
  mount_uploader :logo, DocumentUploader

  validate :file_size_validation

  def file_size_validation
  #errors[:image] << "should be less than xMB" if image.size > 1.megabytes
  #add the following code instead of above line
    if acta.size > 2.megabytes
      errors.add(:acta, "El archivo pesa mas de 2MB")
    end

    if image_rif.size > 0.5.megabytes
      errors.add(:image_rif, "El archivo pesa mas de 500KB")
    end

    if logo.size > 0.5.megabytes
      errors.add(:logo, "Su logo pesa mas de 500KB")
    end
  end

	before_save :add_status 

  before_save :change_status, :if => :status_changed?

    def change_status
      if self.status == "Aprobado"
        password = SecureRandom.hex(4)
        user = User.new( name: self.name, email: self.email_company, password: password, password_confirmation: password, role_ids: 4)
        if user.save
          self.update_attributes user_id: user.id
          SendAffiliate.change_status(self, user).deliver_now 
        end
      end       
    end

	
    def add_status
      self.status= "Pendiente" unless self.created_at?
    end

	
  	def self.index(current_user)
		Affiliate.order('created_at DESC')
	end

	def self.query(query)
      { query: { multi_match:  { query: query, fields: [:status, :name, :rif, :email_company, :phone, :mobile,:names, :email ,:phone_representante ,:mobile_representante ,:acta ,:image_rif], operator: :and }  }, sort: { id: "desc" }, size: Affiliate.count }
    end

    def self.index_total(objects, current_user)
		objects.count
    end

    def self.index_search(objects, current_user)
    	objects.order('created_at DESC')
    end

    def self.route_index
      "/admin/affiliates"
    end

    def self.multiple_destroy(ids, current_user)
      Affiliate.destroy ids
    end

    private

    #validations
    def association_exist
      errors.add(:partners, "(error) Debe agregar al menos 1 socio.") if self.partners.to_a.count.zero? 
      errors.add(:contacts, "(error) Debe agregar al menos 1 dato de contacto.") if self.contacts.to_a.count.zero?
    end

    def partners_count_valid?
      !(partners.reject(&:marked_for_destruction?).count >= 1)
    end

    def contacts_count_valid?
      !(contacts.reject(&:marked_for_destruction?).count >= 1)
    end

    def check_associations_number
      if contacts_count_valid?
        contacts.each { |contact| contact.reload if contact.marked_for_destruction? }
        errors.add(:contacts, "Debe contener al menos 1 registro.")
      end

      if partners_count_valid?
        partners.each { |partner| partner.reload if partner.marked_for_destruction? }
        errors.add(:partners, "Debe contener al menos 1 registro.")
      end
    end
end