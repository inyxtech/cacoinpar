class AffiliatesController < ApplicationController
  before_action :set_affiliate, only: [:show, :edit, :update, :destroy, :send_affiliate]
  before_filter :authenticate_user!, except: [:new, :create]
  load_and_authorize_resource except: [:new, :create]
  layout :resolve_layout
  # GET /affiliates
  # GET /affiliates.json  

  def download_file
    type = params[:type]
    send_file(@affiliate.send(type.to_sym).path,
    :filename => @affiliate.send(type.to_sym).file.filename,
    :type => @affiliate.send(type.to_sym).content_type,
    :disposition => 'attachment',
    :url_based_filename => true)
  end



  # GET /affiliates/1
  # GET /affiliates/1.json
  def show

  end

  # GET /affiliates/new
  def new
    @affiliate = Affiliate.new
    @affiliate.contacts.build
    @affiliate.partners.build
  end

  # GET /affiliates/1/edit
  def edit
  end

  # POST /affiliates
  # POST /affiliates.json
  def create
    @affiliate = Affiliate.new(affiliate_params)

    respond_to do |format|
      if @affiliate.save
        format.html { redirect_to root_path, notice: 'Su afiliación ha sido enviada satisfactoriamente, al ser aprobada, sera enviada su información vía correo electrónico.' }
        format.json { render :show, status: :created, location: @affiliate }
        SendAffiliate.affiliate_email(@affiliate).deliver_now
 
      else
        format.html { render :new }
        format.json { render json: @affiliate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /affiliates/1
  # PATCH/PUT /affiliates/1.json
  def update
    respond_to do |format|
      if @affiliate.update(affiliate_params)
        format.html { redirect_to admin_index_path, notice: 'La afiliacion se ha actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @affiliate }
      else
        format.html { render :edit }
        format.json { render json: @affiliate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /affiliates/1
  # DELETE /affiliates/1.json
  def destroy
    @affiliate.destroy
    respond_to do |format|
      format.html { redirect_to admin_index_path, notice: 'La afiliacion se ha eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affiliate
      @affiliate = Affiliate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affiliate_params
      params.require(:affiliate).permit(:status, :name, :date_construction, :acronym, :rif, :township_construction, :adress, :email_company, :phone, :mobile ,:fax ,:twitter ,:facebook ,:instagram ,:organization ,:service ,:names ,:charges ,:email ,:phone_representante ,:mobile_representante, :whit_representante, :acta ,:image_rif, :logo, :show_lading, contacts_attributes: [ :id , :contact , :charge , :phone,  :_destroy], partners_attributes: [ :id , :manes, :writ, :nationality, :_destroy ])
    end

    def resolve_layout
      case action_name
        when "new", "create"
          "frontend/application"
        else 
          "admin/application"
     end
   end 
end


