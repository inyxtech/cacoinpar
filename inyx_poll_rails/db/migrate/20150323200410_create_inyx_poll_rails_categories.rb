class CreateInyxPollRailsCategories < ActiveRecord::Migration
  def change
    create_table :inyx_poll_rails_categories do |t|
      t.string :name
      t.text :description
      t.string :permalink

      t.timestamps null: false
    end
  end
end
