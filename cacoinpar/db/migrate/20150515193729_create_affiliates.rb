class CreateAffiliates < ActiveRecord::Migration
  def change
    create_table :affiliates do |t|
      t.string :status
      t.boolean :show_lading 
      t.string :name 
      t.string :date_construction 
      t.string :acronym
      t.string :rif 
      t.string :township_construction 
      t.string :adress 
      t.string :email_company 
      t.string :phone 
      t.string :mobile
      t.string :fax
      t.string :twitter
      t.string :facebook
      t.string :instagram
      t.text :organization
      t.text :services
      t.integer :user_id

      t.string :names
      t.string :charges
      t.string :email
      t.string :whit_representante
      t.string :phone_representante
      t.string :mobile_representante

      t.string :acta
      t.string :image_rif
      t.string :logo

		


      t.timestamps null: false
    end
  end
end

    	