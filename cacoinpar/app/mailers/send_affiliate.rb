class SendAffiliate < ApplicationMailer 
  default from: 'no-reply@cacoinpar.org'

	def affiliate_email(affiliate)
		@affiliate = affiliate
		mail(to: 'info@cacoinpar.org', subject: 'Solicitud de nueva afiliacion')
	end

	def change_status(affiliate, user)
		@affiliate = affiliate
		@user = user 
		mail(to: "#{@affiliate.email_company}", subject: 'Su solicitud de afiliacion ha sido aprobada')
	end

end
