class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.string :address
      t.string :phone
      t.belongs_to :affiliate 


      t.timestamps null: false
    end

  end
end
