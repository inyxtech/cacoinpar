require_dependency "inyx_catalog_rails/application_controller"

module InyxCatalogRails
  class CatalogsController < ApplicationController
    before_action :set_catalog, only: [:show, :edit, :update, :destroy]
    layout :resolve_layout
    # GET /catalogs
    
    # GET /catalogs/1
    def show
    end

    # GET /catalogs/new
    def new
      @catalog = Catalog.new 
    end

    # GET /catalogs/1/edit
    def edit
    end

    # POST /catalogs
    def create
      @catalog = Catalog.new(catalog_params)

      if @catalog.save
        redirect_to "/admin/catalogs", notice: 'Catálogo creado satisfactoriamente.'
      else
        render :new
      end
    end

    # PATCH/PUT /catalogs/1
    def update
      if @catalog.update(catalog_params)
        @catalog.__elasticsearch__.index_document
        redirect_to "/admin/catalogs", notice: 'Catálogo actualizado satisfactoriamente.'
      else
        render :edit
      end
    end

    # DELETE /catalogs/1
    def destroy
      @catalog.destroy
      redirect_to catalogs_url, notice: 'Catálogo ha sido borrado satisfactoriamente.'
    end

    def catalog_index
      @catalogs = Catalog.where(public: true).order("created_at DESC").page(params[:page]).per(4)
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_catalog
        @catalog = Catalog.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def catalog_params
        params.require(:catalog).permit(:name, :description, :cover, :category, :public, :image_banner, :show_banner)
      end

    def resolve_layout
      case action_name
        when "catalog_index"
          "frontend/application"
        else 
          "admin/application"
        end
      end
    end

  end

