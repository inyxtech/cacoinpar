require 'test_helper'

module InyxPollRails
  class PollsControllerTest < ActionController::TestCase
    setup do
      @poll = polls(:one)
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:polls)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create poll" do
      assert_difference('Poll.count') do
        post :create, poll: { category_id: @poll.category_id, date_end: @poll.date_end, date_init: @poll.date_init, description: @poll.description, multiple_answer: @poll.multiple_answer, option_id: @poll.option_id, permalink: @poll.permalink, public: @poll.public, question: @poll.question, restricted: @poll.restricted }
      end

      assert_redirected_to poll_path(assigns(:poll))
    end

    test "should show poll" do
      get :show, id: @poll
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @poll
      assert_response :success
    end

    test "should update poll" do
      patch :update, id: @poll, poll: { category_id: @poll.category_id, date_end: @poll.date_end, date_init: @poll.date_init, description: @poll.description, multiple_answer: @poll.multiple_answer, option_id: @poll.option_id, permalink: @poll.permalink, public: @poll.public, question: @poll.question, restricted: @poll.restricted }
      assert_redirected_to poll_path(assigns(:poll))
    end

    test "should destroy poll" do
      assert_difference('Poll.count', -1) do
        delete :destroy, id: @poll
      end

      assert_redirected_to polls_path
    end
  end
end
