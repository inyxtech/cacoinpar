class Partner < ActiveRecord::Base
	belongs_to :affiliate
	validates_presence_of :manes, :nationality
	validates_numericality_of :writ, :allow_blank => true
	validates :writ, :length => { minimum: 7, maximum: 8 }
	validates :writ, :format => {:with => /^[a-zA-Z0-9]*$/, :multiline => true, :message => "No se aceptan caracteres especiales"}

end
