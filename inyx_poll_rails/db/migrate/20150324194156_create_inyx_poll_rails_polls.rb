class CreateInyxPollRailsPolls < ActiveRecord::Migration
  def change
    create_table :inyx_poll_rails_polls do |t|
      t.string :question
      t.text :description
      t.date :date_init
      t.date :date_end
      t.integer :category_id
      t.boolean :multiple_answer
      t.boolean :public
      t.boolean :restricted
      t.string :permalink

      t.timestamps null: false
    end
  end
end
