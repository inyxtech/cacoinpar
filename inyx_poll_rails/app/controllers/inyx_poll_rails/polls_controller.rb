require_dependency "inyx_poll_rails/application_controller"

module InyxPollRails
  class PollsController < ApplicationController
    before_action :set_poll, only: [:show, :edit, :update, :destroy]
    before_action :validation, only: [:create_answer]
    layout :resolve_layout
    load_and_authorize_resource

    # GET /polls/1
    def show
    end

    # GET /polls/new
    def new
      @poll = Poll.new
      @poll.options.build
    end

    # GET /polls/1/edit
    def edit
    end

    # POST /polls
    def create
      @poll = Poll.new(poll_params)

      if @poll.save
        redirect_to @poll, notice: 'Pregunta de la encuesta ha sido creada correctamente'
      else
        render :new
      end
    end

    # PATCH/PUT /polls/1
    def update
      if @poll.update(poll_params)
        @poll.index_elasticsearch_document 
        redirect_to @poll, notice: 'Pregunta de la encuesta ha sido actualizada correcamtente.'
      else
        render :edit
      end
    end

    # DELETE /polls/1
    def destroy
      @poll.destroy
      redirect_to polls_url, notice: 'Pregunta ha sido eliminada correctamente.'
    end

    def front_index
      @polls= Poll.where("public = ? and date_init <= ? and date_end >= ?" , true, Date.today, Date.today)
    end

    def statistics
       @poll= Poll.find_by_permalink(params[:permalink])
    end


    private
      # Use callbacks to share common setup or constraints between actions.
      def set_poll
        @poll = Poll.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def poll_params
        params.require(:poll).permit(:permalink_option, :question, :description, :date_init, :date_end, :category_id, :multiple_answer, :public, :restricted, :permalink, options_attributes: [:id, :answer, :_destroy])
      end

      def resolve_layout
        case action_name
        when "new", "create", "destroy", "update", "edit", "show", "index", "statistics"
          'admin/application'
        else
          'frontend/application'
        end
      end

      def validation
        @poll = Poll.find_by_permalink(params[:poll][:permalink_option])
        if Poll.find(@poll.id).answers.find_by ip: request.remote_ip
          raise CanCan::AccessDenied.new("Ya ha votado en esta encuesta", "Error", InyxPollRails::Answer)
        end
      end

  end
end
