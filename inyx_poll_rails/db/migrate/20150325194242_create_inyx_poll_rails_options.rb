class CreateInyxPollRailsOptions < ActiveRecord::Migration
  def change
    create_table :inyx_poll_rails_options do |t|
      t.string :answer
      t.integer :poll_id

      t.timestamps null: false
    end
  end
end
