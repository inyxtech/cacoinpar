# desc "Explaining what the task does"
# task :inyx_poll_rails do
#   # Task goes here
# end
require File.expand_path('../../inyx_poll_rails/tasks/install', __FILE__)

namespace :poll do
	desc "Copiar inicializador para la configuración"
	task :copy_initializer do
		InyxPollRails::Tasks::Install.copy_initializer_file
	end

  desc "Copiar vistas al proyecto"
  task :copy_views do
    InyxPollRails::Tasks::Install.copy_view_files
  end
end
