class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :contact
      t.string :charge
      t.string :phone
      t.belongs_to :affiliate 



      t.timestamps null: false
    end

  end
end
